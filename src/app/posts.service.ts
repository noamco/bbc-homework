import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs';
import { getMultipleValuesInSingleSelectionError } from '@angular/cdk/collections';
import { PostsRaw } from './interfaces/posts-raw';
import { Posts } from './interfaces/posts';
import { Users } from './interfaces/users';

@Injectable({
  providedIn: 'root'
})
export class PostsService {

  apiUrl = 'https://jsonplaceholder.typicode.com/posts/';
  apiUrl2= 'https://jsonplaceholder.typicode.com/users';

  constructor(private _http: HttpClient) {
    
   }
   getPosts() {
    return this._http.get<Posts[]>(this.apiUrl);
  }
  getUsers() {
    return this._http.get<Users[]>(this.apiUrl2);
  }

}