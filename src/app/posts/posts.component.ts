import { Component, OnInit } from '@angular/core';
import { Posts } from './../interfaces/posts';
import { Observable } from 'rxjs';
import { PostsService } from './../posts.service';
import { ActivatedRoute } from '@angular/router';
import { Users } from '../interfaces/users';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {
  posts$: Posts[];
  users$: Users[];
  constructor(private PostsService: PostsService) { }

  ngOnInit() {
    this.PostsService.getPosts()
      .subscribe(posts => this.posts$ = posts);
    this.PostsService.getUsers()
      .subscribe(users => this.users$ = users);
  }

}