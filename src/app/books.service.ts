import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class BooksService {
  books:any= [{id:1, title:'Alice in Wonderland', author:'Lewis Carrol'},{id:2, title:'War and Peace', author:'Leo Tolstoy'}, {id:3, title:'The Magic Mountain', author:'Thomas Mann'}];

  getBooks(){
    setInterval(()=>this.books,1000)
}
  constructor() { }
}
