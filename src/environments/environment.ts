// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.


export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyCvlyrhaOlte-sEk4h8KEaK7BDk1ROb1Bo",
    authDomain: "bbc-hw-noamco.firebaseapp.com",
    databaseURL: "https://bbc-hw-noamco.firebaseio.com",
    projectId: "bbc-hw-noamco",
    storageBucket: "bbc-hw-noamco.appspot.com",
    messagingSenderId: "926745544214",
    appId: "1:926745544214:web:572ccaac753bf006ea9737"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
